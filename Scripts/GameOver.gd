extends Control

onready var record = $MarginContainer/VBoxContainer/VBoxContainer/Record
onready var fire = [$MarginContainer/VBoxContainer/VBoxContainer/Sprite/Fire, $MarginContainer/VBoxContainer/VBoxContainer/Sprite/Fire2]
onready var player = $MarginContainer/VBoxContainer/VBoxContainer/Sprite/Sprite
onready var gameover_text = $MarginContainer/VBoxContainer/Label
onready var sub_text = $MarginContainer/VBoxContainer/Label2

# Called when the node enters the scene tree for the first time.
func _ready():
	VisualServer.set_default_clear_color(Color(0,0,0))
	Main.musicHandle("gameover")
	getText()

func getText():
	var distance = Main.getDistance()
	var type = Main.getType()
	var rec_text = "You made it %.2f miles." % distance
	record.text = rec_text
	if type == "armor":
		for flame in fire:
			flame.set_visible(true)
	elif type == "win":
		player.set_visible(true)
		gameover_text.text = "Congratulations!"
		sub_text.text = "You made it! Get ready to do it again tomorrow!"


func _on_MainMenu_pressed():
	$AudioStreamPlayer.play()
	Main.mainMenu()
