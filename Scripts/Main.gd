extends Node2D

var gameover = "res://Scenes/GameOver.tscn"
var main_menu = "res://Scenes/MainMenu.tscn"
var instructions_scene = "res://Scenes/Instructions.tscn"
var road = "res://Scenes/Road.tscn"

var difficulty

var hitch = 0.0
var no_hitch = true

var gameover_type
var distance
# Called when the node enters the scene tree for the first time.
func _ready():
#	$Tween.interpolate_method(Engine, "set_time_scale", 0.2, 1.0, .4, 0, 3)
	set_process(true)


func _process(delta):
#	if (hitch > 0.0):
#		hitch -= delta
#		if (hitch <= 0.0 and !no_hitch):
#			no_hitch = true
#			$Tween.start()
	pass

func gameOver(type, record):
	gameover_type = type
	distance = record
	get_tree().change_scene(gameover)

func musicHandle(scene):
	$DrivingMusic.stop()
	if (scene != "menu"):
		$MenuMusic.stop()
	$GameoverMusic.stop()
	if scene == "road":
		$DrivingMusic.play()
	elif scene == "menu" and !$MenuMusic.is_playing():
		$MenuMusic.play()
	elif scene == "gameover":
		$GameoverMusic.play()

func getType():
	return gameover_type

func getDistance():
	return distance

func mainMenu():
	get_tree().change_scene(main_menu)

func instructions():
	get_tree().change_scene(instructions_scene)

func start(get_diff):
	if get_diff == "Easy":
		difficulty = load("res://Scripts/Easy.gd").new()
	if get_diff == "Medium":
		difficulty = load("res://Scripts/Medium.gd").new()
	if get_diff == "Hard":
		difficulty = load("res://Scripts/Hard.gd").new()
	get_tree().change_scene(road)

func getDifficulty():
	return difficulty

#func contactHitch():
#	Engine.set_time_scale(0.2)
#	no_hitch = false
#	hitch = 0.1

