extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	Main.musicHandle("menu")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Instructions_pressed():
	$AudioStreamPlayer.play()
	Main.instructions()


func _on_Easy_pressed():
	$AudioStreamPlayer.play()
	Main.start("Easy")


func _on_Medium_pressed():
	$AudioStreamPlayer.play()
	Main.start("Medium")

func _on_Hard_pressed():
	$AudioStreamPlayer.play()
	Main.start("Hard")