extends Node

var traffic_chance = 25
var max_speed = 15
var target_dist = 10
var wander_chance = 10
var widen_chance = [6, 10]
var narrow_chance = [11, 16]
var road_timer = 7
var speed_increase = false
var car_max = 30
var charge_rate = .75
var gas_drain = .015

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func getTrafficChance():
	return traffic_chance

func getMaxSpeed():
	return max_speed

func getTargetDist():
	return target_dist

func getWanderChance():
	return wander_chance

func getWidenChance():
	return widen_chance

func getNarrowChance():
	return narrow_chance

func getRoadTimer():
	return road_timer

func speedIncreases():
	return speed_increase

func getCarMax():
	return car_max

func getChargeRate():
	return charge_rate

func getGasDrain():
	return gas_drain