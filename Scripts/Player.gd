extends KinematicBody2D

var max_speed = 5
var accel = .75
var vert_drag = .2
var drag_value = .8
var drag = false
var gas_drain = .005
var armor_drain = 10
var laser_charge = 2
var elapsed = 0.0

var ray_array = Array()
var move_vect
var move_down = false
var move_up = false
var ui
var env
var target
var no_gas = false

signal gameover_main


func _ready():
	set_process(true)
	move_vect = Vector2(0,0)
	var array_num = 0
	var angle = -27.0
	while array_num < 90:
		var new_array = RayCast2D.new()
		new_array.set_cast_to(Vector2(300,0))
		new_array.set_enabled(true)
		new_array.rotation_degrees = angle
		angle += 0.6
		add_child(new_array)
		ray_array.append(weakref(new_array))
		array_num += 1
	connect("gameover_main", Main, "gameOver")

func _unhandled_input(event):
	if(event.is_action_pressed("ui_down")):
		move_down = true
	if(event.is_action_pressed("ui_up")):
		move_up = true
	if(event.is_action_released("ui_down")):
		move_down = false
	if(event.is_action_released("ui_up")):
		move_up = false
	if(event.is_action_pressed("ui_accept") and getTargetState() != null and ui.getLasersLeft() > 0):
		laserAction()
		ui.laserFired()

func _process(delta):
	print (move_vect.x)
	ui.setGas(gas_drain)
	ui.setCharge(ui.getCharge() + laser_charge)
	if (move_down):
		$Sprite.set_rotation_degrees(max(get_rotation_degrees() + 1, 25))
		move_vect.y += 3 * move_vect.x / 3
		move_vect.y = max(move_vect.y, 0.3)
	if (move_up):
		$Sprite.set_rotation_degrees(max(get_rotation_degrees() - 1, -25))
		move_vect.y -= 3 * move_vect.x / 3
		move_vect.y = min(move_vect.y, -0.3)
	else:
		$Sprite.set_rotation_degrees(0)
	move_vect.y = clamp(move_vect.y, -1.5, 1.5)
	if (move_vect.y != 0):
		if (move_vect.y < .2 and move_vect.y > -.2):
			move_vect.y = 0
		else:
			if (move_vect.y > 0):
				move_vect.y -= vert_drag
			elif (move_vect.y < 0):
				move_vect.y += vert_drag
	if (drag):
		move_vect.x -= drag_value
		move_vect.x = max(move_vect.x, 0.0)
	move_vect.x += delta * accel
	move_vect.x = min(move_vect.x, max_speed)
	if no_gas == true and move_vect.x <= 0.0:
		emit_signal("gameover_main")
	var has_collided = move_and_collide(move_vect)
	if (has_collided):
		if (has_collided.get_collider()):
#			print('i hit ' + has_collided.get_collider().name)
#			print('in group ' + str(has_collided.get_collider().get_groups()))
			if (has_collided.get_collider().is_in_group("Traffic")):
				has_collided.get_collider().die(move_vect)
				move_vect.x /= 1.3
				ui.setArmor(ui.getArmor() - armor_drain)
	for ray in ray_array:
		if (ray.get_ref().is_colliding()):
			if (ray.get_ref().get_collider().is_in_group("Traffic")):
				if (target == null):
					target = weakref(ray.get_ref().get_collider())
					target.get_ref().toggleCrosshair()
				if (!target.get_ref()):
					target = weakref(ray.get_ref().get_collider())
					target.get_ref().toggleCrosshair()
				elif (ray.get_ref().get_collider().position.distance_to(position) < target.get_ref().position.distance_to(position)):
					target.get_ref().toggleCrosshair()
					target = weakref(ray.get_ref().get_collider())
					target.get_ref().toggleCrosshair()
	if (target != null):
		if (target.get_ref()):
			if (target.get_ref().position.x <= position.x):
				target.get_ref().toggleCrosshair()
				target = null
	elapsed += delta
	max_speed = 10 + pow(1.1, elapsed) 

func enableDrag():
	drag = true

func disableDrag():
	drag = false

func setContexts(interface, road):
	ui = interface
	env = road
	ui.addPlayer(self)

func getTargetState():
	if (target == null):
		return null
	return target.get_ref()

func laserAction():
	var laser = load("res://Scenes/Laser.tscn").instance()
	var points = PoolVector2Array()
	points.append(to_global(position))
	points.append(to_global(target.get_ref().position))
	target.get_ref().shot()
	laser.get_node("Line2D").set_points(points)
	add_child(laser)

func gameOver(type):
	if type == "gas":
		drag_value = 10
		no_gas = true
	if type == "armor"