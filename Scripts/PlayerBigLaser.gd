extends KinematicBody2D

var max_speed = 5
var accel = .75
var vert_drag = .2
var drag_value = .8
var drag = false
var gas_drain = .025
var armor_drain = 10
var laser_charge = .2
var elapsed = 0.0

var laser_targets = Array()
var move_vect
var move_down = false
var move_up = false
var sunup_emitted = false
var ui
var env
var target
var no_gas = false
var distance = 0.0
var target_dist = 3
var speed_increasing = false
var difficulty

signal gameover_main(type, record)
signal sunup

func _ready():
	$Sprite.frame = 716
	set_process(true)
	move_vect = Vector2(0,0)
	connect("gameover_main", Main, "gameOver")

func _unhandled_input(event):
	if(event.is_action_pressed("ui_down")):
		move_down = true
	if(event.is_action_pressed("ui_up")):
		move_up = true
	if(event.is_action_released("ui_down")):
		move_down = false
	if(event.is_action_released("ui_up")):
		move_up = false
	if(event.is_action_pressed("ui_accept") and ui.getLasersLeft() > 0):
		laserAction()
		ui.laserFired()

func _process(delta):
	if(!drag and $SkidNoise.is_playing()):
		$SkidNoise.stop()
	ui.setGas(gas_drain)
	ui.setCharge(ui.getCharge() + laser_charge)
	if (move_down):
		$Sprite.set_rotation_degrees(max(get_rotation_degrees() + 1, 25))
		move_vect.y += 3 * move_vect.x / 3
		move_vect.y = max(move_vect.y, 0.3)
	if (move_up):
		$Sprite.set_rotation_degrees(max(get_rotation_degrees() - 1, -25))
		move_vect.y -= 3 * move_vect.x / 3
		move_vect.y = min(move_vect.y, -0.3)
	else:
		$Sprite.set_rotation_degrees(0)
	move_vect.y = clamp(move_vect.y, -1.5, 1.5)
	if (move_vect.y != 0):
		if (move_vect.y < .2 and move_vect.y > -.2):
			move_vect.y = 0
		else:
			if (move_vect.y > 0):
				move_vect.y -= vert_drag
			elif (move_vect.y < 0):
				move_vect.y += vert_drag
	if (drag):
		if(!$SkidNoise.is_playing()):
			$SkidNoise.play()
		move_vect.x -= drag_value
		move_vect.x = max(move_vect.x, 0.0)
	move_vect.x += delta * accel
	move_vect.x = min(move_vect.x, max_speed)
	distance += move_vect.x / 3000
	if (distance >= (target_dist * .75) and !sunup_emitted):
		emit_signal("sunup")
		sunup_emitted = true
	if (distance >= target_dist):
		$AudioStreamPlayer.stop()
		emit_signal("gameover_main", "win", distance)
	if no_gas == true:
		drag = true
		if move_vect.x / delta <= 1.0:
			$AudioStreamPlayer.stop()
			emit_signal("gameover_main", "gas", distance)
	var has_collided = move_and_collide(move_vect)
	if (has_collided):
		if (has_collided.get_collider()):
#			print('i hit ' + has_collided.get_collider().name)
#			print('in group ' + str(has_collided.get_collider().get_groups()))
			if (has_collided.get_collider().is_in_group("Traffic")):
#				Main.contactHitch()
				has_collided.get_collider().die(move_vect)
				$AudioStreamPlayer3.play()
				move_vect.x /= 1.3
				ui.setArmor(ui.getArmor() - armor_drain)
	if (speed_increasing == true):
		elapsed += delta
		max_speed = 10 + pow(1.1, elapsed) 

func enableDrag():
	drag = true

func disableDrag():
	drag = false

func setContexts(interface, road):
	ui = interface
	env = road
	ui.getPlayer(self)

func laserAction():
	var laser = load("res://Scenes/Laser.tscn").instance()
	laser.position.x += 20
	$LaserSound.play()
	$AudioStreamPlayer3.play()
	for target in laser_targets:
		target.get_ref().shot()
	add_child(laser)
	

func _on_Area2D_body_entered(body):
	laser_targets.append(weakref(body))


func _on_Area2D_body_exited(body):
	for target in laser_targets:
		if target.get_ref() == body:
			laser_targets.erase(target)

func gameOver(type):
	if type == "gas":
		drag_value = accel * 1.1
		no_gas = true
	if type == "armor":
		$AudioStreamPlayer.stop()
		$AudioStreamPlayer2.play()
		$AnimationPlayer.play("Explode")

func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("gameover_main", "armor", distance)

func attachSunup(scene):
	connect("sunup", scene, "startSunup")

func setDifficulty(script):
	difficulty = script
	speed_increasing = difficulty.speedIncreases()
	max_speed = difficulty.getMaxSpeed()
	target_dist = difficulty.getTargetDist()
	laser_charge = difficulty.getChargeRate()
	gas_drain = difficulty.getGasDrain()