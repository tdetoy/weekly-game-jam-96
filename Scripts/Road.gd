extends Node2D

onready var road_segment = load("res://Scenes/RoadSegmentSmall.tscn")
onready var traffic_scene = load("res://Scenes/Traffic.tscn")
onready var explosion_trail = load("res://Scenes/Trail.tscn")

var street_segments = Array()
var traffic_instances = Array()
var ornaments = Array()

var difficulty

var x_pos : float
var y_pos : float
var car_x
var counter = 500
var width
var wandered = false
var widened = false
var narrowed = false
var road_timer = 3.0
var traffic_proc
var narrow_proc
var widen_proc
var wander_proc
var car_max

onready var player = $Player
onready var camera = $Camera2D

func _ready():
	Main.musicHandle("road")
	setDifficulty()
	$Tween.interpolate_method(VisualServer, "set_default_clear_color", Color(0, 0, 0), Color(.33, .20, .11), 7, 0, 3)
	player.setContexts($Control, self)
	player.attachSunup(self)
	randomize()
	set_process(true)
	var start = self.to_global(camera.get_camera_position())
	x_pos = start.x - 500
	y_pos = start.y
	width = 50
	while x_pos < start.x + ProjectSettings.get_setting("display/window/size/width") + 250:
			var road_node = road_segment.instance()
			road_node.roadDistance(width)
			road_node.set_position(self.to_global(Vector2(x_pos, y_pos)))
			x_pos += road_node.get_node("Node2D/Divider").get_rect().size.x * 2
			add_child(road_node)
			street_segments.append(weakref(road_node))
			print(street_segments.size())
	car_x = x_pos

func _process(delta):
	counter -= 1
	camera.position.x = player.position.x + 400
	if camera.position.y < player.position.y - 50:
		camera.position.y = player.position.y - 50
	elif camera.position.y > player.position.y + 50:
		camera.position.y = player.position.y + 50
	if counter == 0:
		counter = 500
		print("street segment array: " + str(street_segments.size()))
		print("traffic instances: " + str(traffic_instances.size()))
		print("num children: " + str(get_child_count()))
	
	crawlRefList(street_segments)
	crawlRefList(traffic_instances)
	crawlRefList(ornaments)
	
	if (road_timer <= 0.0):
		var change = randi() % 50
		if change <= wander_proc and !wandered:
			var result = roadWander()
			widened = false
			narrowed = false
			wandered = true
		elif change <= widen_proc[1] and change > widen_proc[0] and !widened:
			var result = roadWidth("widen")
			narrowed = false
			widened = true if randi() % 2 == 0 else true
		elif change <= narrow_proc[1] and change > narrow_proc[0] and !narrowed:
			var result = roadWidth("narrow")
			widened = false
			narrowed = true if randi() % 2 == 0 else true
		road_timer = difficulty.getRoadTimer()
	var wander = randi() % 200
	if wander <= 5 and !wandered:
		var result = roadWander()
		widened = false
		narrowed = false
		wandered = true
	var traffic_chance = randi() % 20 + (width - 100)
	if (traffic_chance <= traffic_proc and traffic_instances.size() < (car_max + (width - 30))):
		spawnCar()
	if (street_segments.size() < 500):
		placeRoad()
		wandered = false
		widened = false
		narrowed = false
	road_timer -= delta
	var ornament_chance = randi() % 100
	if ornament_chance > 50 and ornaments.size() < 500:
		placeTile()
	elif ornament_chance < 5 and ornaments.size() < 500:
		placeFeature()
	if car_x < to_global(camera.position).x + ProjectSettings.get_setting("display/window/size/width"):
		car_x = to_global(camera.position).x + ProjectSettings.get_setting("display/window/size/width") + 100

func roadWander():
	randomize()
	var multiplier
	var cont = false
	var result = randi() % 2
	var drift = ((randi() % 5) + 1) * 5
	if (result == 0 and y_pos < self.to_global(camera.get_camera_position()).y + ProjectSettings.get_setting("display/window/size/height") + width * 2 + drift):
		multiplier = 1
		cont = true
	elif (result == 1 and y_pos > self.to_global(camera.get_camera_position()).y - width * 2 - drift):
		multiplier = -1
		cont = true
	if (cont):
		var new_nodes = 0
		while new_nodes < 5:
			y_pos += multiplier * drift / 5
			placeRoad()
			new_nodes += 1

func placeRoad():
	var road_node = road_segment.instance()
	road_node.roadDistance(width)
	road_node.set_position(Vector2(x_pos, y_pos))
	street_segments.append(weakref(road_node))
	x_pos += road_node.get_node("Node2D/Divider").get_rect().size.x * 2
	add_child(road_node)


func roadWidth(operation):
	print("changing width")
	randomize()
	var amount = randf() * 100
	var new_nodes = 0
	while new_nodes < 10:
		if (operation == "widen" and width < 200):
			width += amount / 10
		elif (operation == "narrow" and width > 30):
			width -= amount / 10
		placeRoad()
		new_nodes += 1

func spawnCar():
	print("creating car")
	var car_y
	car_y = y_pos	
	var query = get_world_2d().direct_space_state.intersect_ray(Vector2(car_x, car_y), Vector2(car_x, car_y + 100), [], 2147483647, false, true)
	if (!query.has('collider') or query.get('collider') == null ):
		query = get_world_2d().direct_space_state.intersect_ray(Vector2(car_x, car_y), Vector2(car_x, car_y + 100), [], 2147483647, false, true)
	if (query.get('collider') != null):
		var node = query.get('collider')
		while !node.name.match("*RoadSegment*"):
			node = node.get_owner()
		car_y = node.position.y
		randomize()
		var multiplier = 1 if randi() % 2 == 0 else -1
		car_y += multiplier * width * randf()
		var car_node = traffic_scene.instance()
		car_node.set_position(Vector2(car_x, car_y))
		traffic_instances.append(weakref(car_node))
		car_node.createSignal(self)
		if (randi() % 10 < 3):
			car_node.setSpeed(0)
		elif (car_y < y_pos):
			car_node.setSpeed(-40)
		add_child(car_node)
		car_x += car_node.get_node("Sprite").get_rect().size.x + (randf() * 10)

func createTrail(trail_position):
	var trail = explosion_trail.instance()
	trail.position = trail_position
	add_child(trail)

func placeFeature():
	var ruined_house = load("res://Scenes/RuinedHouse.tscn").instance()
	var ruined_building = load("res://Scenes/RuinedBuilding.tscn").instance()
	var ruined_pile = load("res://Scenes/RuinedPile.tscn").instance()
	
	randomize()
	var object = randi() % 3
	match object:
		0:
			ruined_house.position = Vector2(x_pos, y_pos)
			var y_drift = (width + 125 + randi() % 20)
			var up_down = randi() % 2
			if up_down == 0:
				y_drift *= -1
			ruined_house.position.y += y_drift
			ruined_house.position.x += (randi() % 5) * 1 if randi() % 2 == 0 else -1
			add_child(ruined_house)
			ornaments.append(weakref(ruined_house))
		1:
			ruined_building.position = Vector2(x_pos, y_pos)
			var y_drift = (width + 125 + randi() % 20)
			var up_down = randi() % 2
			if up_down == 0:
				y_drift *= -1
			ruined_building.position.y += y_drift
			ruined_building.position.x += (randi() % 5) * 1 if randi() % 2 == 0 else -1
			add_child(ruined_building)
			ornaments.append(weakref(ruined_building))
		2:
			ruined_pile.position = Vector2(x_pos, y_pos)
			var y_drift = (width + 125 + randi() % 20)
			var up_down = randi() % 2
			if up_down == 0:
				y_drift *= -1
			ruined_pile.position.y += y_drift
			ruined_pile.position.x += (randi() % 5) * 1 if randi() % 2 == 0 else -1
			add_child(ruined_pile)
			ornaments.append(weakref(ruined_pile))

func placeTile():
	var cactus = load("res://Scenes/Cactus.tscn").instance()
	var ground_tile = load("res://Scenes/GroundTile.tscn").instance()
	
	randomize()
	var chance = randi() % 20
	if (chance > 5):
		ground_tile.position = Vector2(x_pos, y_pos)
		var y_drift = (width + 50 + randi() % 20)
		var up_down = randi() % 2
		if up_down == 0:
			y_drift *= -1
		ground_tile.position.y += y_drift
		ground_tile.position.x += (randi() % 5) * 1 if randi() % 2 == 0 else -1
		add_child(ground_tile)
		ornaments.append(weakref(ground_tile))
	else:
		cactus.position = Vector2(x_pos, y_pos)
		var y_drift = (width + 50 + randi() % 20)
		var up_down = randi() % 2
		if up_down == 0:
			y_drift *= -1
		cactus.position.y += y_drift
		cactus.position.x += (randi() % 5) * 1 if randi() % 2 == 0 else -1
		add_child(cactus)
		ornaments.append(weakref(cactus))

func crawlRefList(list):
#	print("erasing from " + str(list))
	for ref in list:
		if ref.get_ref():
			if self.to_global(ref.get_ref().get_position()).x < self.to_global(camera.get_camera_position()).x - 500:
				ref.get_ref().propagate_call("queue_free", [])
				list.erase(ref)
				if(list == traffic_instances):
					print("erasing:")
					print(ref.get_ref().name)
				ref.get_ref().queue_free()
			elif ref.get_ref().is_queued_for_deletion():
				list.erase(ref)
				if(list == traffic_instances):
					print("queued_for_delete, erasing")
					print(ref.get_ref().name)
		else:
			list.erase(ref)
			print("empty, erased:")
			print(ref)

func startSunup():
	$Tween.start()

func setDifficulty():
	difficulty = Main.getDifficulty()
	narrow_proc = difficulty.getNarrowChance()
	widen_proc = difficulty.getWidenChance()
	wander_proc = difficulty.getWanderChance()
	traffic_proc = difficulty.getTrafficChance()
	road_timer = difficulty.getRoadTimer()
	car_max = difficulty.getCarMax()
	player.setDifficulty(difficulty)
	