extends Node2D

func _ready():
	pass

func roadDistance(distance = 0):
	if (distance == 0):
		randomize()
		distance = randi() % 100 + 100
	$Node2D/TopRoad.position.y -= distance
	$Node2D/BottomRoad.position.y += distance
	$TopSegment/CollisionShape2D.position.y -= distance / 2
	$BottomSegment/CollisionShape2D.position.y += distance /2
	$TopSegment/CollisionShape2D.get_shape().set_extents(Vector2(8.5, (distance + 30) / 4))
	$DividerSegment/CollisionShape2D.get_shape().set_extents(Vector2(8.5, (distance + 30) / 4))
	$BottomSegment/CollisionShape2D.get_shape().set_extents(Vector2(8.5, (distance + 30) / 4))
	$TopExtent.position.y -= distance
	$BottomExtent.position.y += distance


func _on_TopBoundary_body_entered(body):
	if (body.name == "Player"):
		body.enableDrag()
	elif (body.is_in_group("Traffic")):
		body.die(Vector2(0,0))


func _on_BottomBoundary_body_entered(body):
	if (body.name == "Player"):
		body.enableDrag()
	elif (body.is_in_group("Traffic")):
		body.die(Vector2(0,0))

func _on_BottomBoundary_body_exited(body):
	if (body.name == "Player"):
		body.disableDrag()


func _on_TopBoundary_body_exited(body):
	if (body.name == "Player"):
		body.disableDrag()


func _on_TopExtent_body_entered(body):
	if (body.name == "Player"):
		body.enableDrag()
	elif (body.is_in_group("Traffic")):
		body.die(Vector2(0,0))


func _on_TopExtent_body_exited(body):
	if (body.name == "Player"):
		body.enableDrag()
	elif (body.is_in_group("Traffic")):
		body.die(Vector2(0,0))


func _on_BottomExtent_body_exited(body):
	if (body.name == "Player"):
		body.disableDrag()


func _on_BottomExtent_body_entered(body):
	if (body.name == "Player"):
		body.disableDrag()
