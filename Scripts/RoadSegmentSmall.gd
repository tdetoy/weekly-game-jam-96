extends Node2D

var segment_list = Array()

func _ready():
	pass

func roadDistance(distance = 0):
	if (distance == 0):
		randomize()
		distance = randi() % 100 + 100
	for node in segment_list:
		node.get_node("Node2D/TopRoad").position.y -= distance
		node.get_node("Node2D/BottomRoad").position.y += distance
