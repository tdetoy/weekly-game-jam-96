extends KinematicBody2D

var moving = 0
var trail_countdown = 0.05
var prev_area
var collide_vect
var player_collided
var speed = 20

onready var ray = $RayCast2D
onready var anim = $AnimationPlayer

signal trail_create(position)

func _ready():
	randomize()
	var look = randi() % 5
	match look:
		0:
			$Sprite.frame = 715
		1:
			$Sprite.frame = 717
		2:
			$Sprite.frame = 747
		3:
			$Sprite.frame = 748
		4:
			$Sprite.frame = 749
	set_process(true)

func _process(delta):
	if (!player_collided):
		var y_component = 0
		if (ray.is_colliding()):
			determineCollision(ray.get_collider())
			set_rotation_degrees(moving * 15)
			y_component = moving * 9
		var collide_vect = move_and_collide(Vector2(1,y_component) * speed * delta)
		if (collide_vect):
			if (collide_vect.get_collider()):
				if (collide_vect.get_collider().is_in_group("Traffic")):
						move_and_collide(Vector2(1, 1) * speed * delta)
				elif (collide_vect.get_collider().is_class("Area2D")):
					if (collide_vect.get_collider().name == "BottomExtent" or\
					collide_vect.get_collider().name == "TopExtent" or\
					collide_vect.get_collider().name == "BottomBoundary" or\
					collide_vect.get_collider().name == "TopBoundary"):
						queue_free()

	else:
		move_and_collide(collide_vect * delta)
		trail_countdown -= delta
		if (trail_countdown <= 0.0):
			trail_countdown = 0.05
			emit_signal("trail_create", position)

func determineCollision(area):
	if (area):
		if (!prev_area):
			prev_area = area.name
		elif (prev_area != area.name):
			if (prev_area == "TopSegment"):
				if (area.name == "DividerSegment"):
					moving = -1
				elif (area.name == "TopBoundary"):
					moving = 1
			elif (prev_area == "DividerSegment"):
				if (area.name == "TopSegment"):
					moving = 1
				elif (area.name == "BottomSegment"):
					moving = -1
			elif (prev_area == "BottomSegment"):
				if (area.name == "DividerSegment"):
						moving = 1
				elif (area.name == "BottomBoundary"):
					moving = -1
		elif (prev_area == area.name):
			moving = 0

func die(player_vect):
	randomize()
	print('i got hit')
	if ($CollisionShape2D != null):
		$CollisionShape2D.set_deferred("disabled", true)
	collide_vect = player_vect
	collide_vect.x += pow(collide_vect.x, 3)
	collide_vect.y += pow(collide_vect.y, 3)
	player_collided = true
	anim.play("Explode")

func setFire():
	var fire = load("res://Scenes/Fire.tscn").instance()
	fire.position = Vector2(0, -5)
	add_child(fire)

func _on_AnimationPlayer_animation_finished(anim_name):
	self.queue_free()

func createSignal(scene):
	connect("trail_create", scene, "createTrail")

func toggleCrosshair():
	$Crosshair.set_visible(!$Crosshair.is_visible())

func shot():
	setFire()
	$CollisionShape2D.set_deferred("disabled", true)
	die(Vector2(0,0))

func setSpeed(val):
	speed = val