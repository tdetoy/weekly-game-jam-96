extends CanvasLayer

onready var charge_bar = $MarginContainer/VBoxContainer/HBoxContainer/ChargeBar
onready var gas_bar = $MarginContainer/VBoxContainer/HBoxContainer2/GasBar
onready var armor_bar = $MarginContainer/VBoxContainer/HBoxContainer3/ArmorBar

var gas_value = 100.0

var laser_list = Array()
var lasers_left = 0
var player

signal gameover(type)

# Called when the node enters the scene tree for the first time.
func _ready():
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser1/Sprite)
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser2/Sprite)
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser3/Sprite)
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser4/Sprite)
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser5/Sprite)
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser6/Sprite)
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser7/Sprite)
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser8/Sprite)
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser9/Sprite)
	laser_list.append($MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/Laser10/Sprite)
	
	gas_bar.value = floor(gas_value)
	armor_bar.value = 100

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func setArmor(set_value):
	armor_bar.value = set_value
	if (armor_bar.value <= 0):
		emit_signal("gameover", "armor")

func getArmor():
	return armor_bar.value

func setGas(delta):
	gas_value -= delta
	gas_bar.value = floor(gas_value)
	if (gas_bar.value <= 0):
		emit_signal("gameover", "gas")

func getGas():
	return gas_bar.value

func setCharge(set_value):
	if (lasers_left < 10):
		charge_bar.value = set_value
	if charge_bar.value >= 100 and lasers_left < 10:
		lasers_left += 1
		laser_list[lasers_left - 1].frame = 826
		charge_bar.value = 0

func laserFired():
	laser_list[lasers_left - 1].frame = 825
	lasers_left -= 1

func getLasersLeft():
	return lasers_left

func getCharge():
	return charge_bar.value

func getPlayer(node):
	player = node
	connect("gameover", player, "gameOver")